#!/bin/bash

. ~/.config/bar/config

sep="%{F$COLOR_DISABLED} | %{F-}"
num_mon=$(bspc query -M | wc -l)
pwr="%{A1:powermenu:}%{A2:powermenu poweroff:}%{A3:powermenu suspend:}%{F$COLOR_ALERT}PWR%{F-}%{A}%{A}%{A}"

while read -r line; do
	case "$line" in
		B*)
			battery="${line#?}"
			;;
		T*)
			title="${line#?}"
			;;
		S*)
			clock="${line#?}"
			;;
		L*)
			backlight="${line#?}"
			;;
		V*)
			volume="${line#?}"
			;;
		N*)
			network="${line#?}"
			;;
		W*)
			# bspwm's state
			wm=
			IFS=':'
			set -- ${line#?}
			while [ $# -gt 0 ] ; do
				item=$1
				name=${item#?}
				case $item in
					[mM]*)
						case $item in
							m*)
								# monitor
								FG=$COLOR_MONITOR_FG
								BG=$COLOR_MONITOR_BG
								on_focused_monitor=
								;;
							M*)
								# focused monitor
								FG=$COLOR_FOCUSED_MONITOR_FG
								BG=$COLOR_FOCUSED_MONITOR_BG
								on_focused_monitor=1
								;;
						esac
						[ $num_mon -lt 2 ] && shift && continue
						wm="${wm}%{F${FG}}%{B${BG}}%{A:bspc monitor -f ${name}:} ${name} %{A}%{B-}%{F-}"
						;;
					[fFoOuU]*)
						case $item in
							f*)
								# free desktop
								FG=$COLOR_DISABLED
								BG=$COLOR_BACKGROUND
								UL=$COLOR_BACKGROUND
								;;
							F*)
								if [ "$on_focused_monitor" ] ; then
									# focused free desktop
									FG=$COLOR_FOREGROUND
									BG=$COLOR_BACKGROUND_ALT
									UL=$COLOR_PRIMARY
								else
									# active free desktop
									FG=$COLOR_DISABLED
									BG=$COLOR_BACKGROUND
									UL=$COLOR_BACKGROUND
								fi
								;;
							o*)
								# occupied desktop
								FG=$COLOR_FOREGROUND
								BG=$COLOR_BACKGROUND
								UL=$COLOR_BACKGROUND
								;;
							O*)
								if [ "$on_focused_monitor" ] ; then
									# focused occupied desktop
									FG=$COLOR_FOREGROUND
									BG=$COLOR_BACKGROUND_ALT
									UL=$COLOR_PRIMARY
								else
									# active occupied desktop
									FG=$COLOR_FOREGROUND
									BG=$COLOR_BACKGROUND
									UL=$COLOR_BACKGROUND
								fi
								;;
							u*)
								# urgent desktop
								FG=$COLOR_DISABLED
								BG=$COLOR_ALERT
								UL=$COLOR_ALERT
								;;
							U*)
								if [ "$on_focused_monitor" ] ; then
									# focused urgent desktop
									FG=$COLOR_FOREGROUND
									BG=$COLOR_ALERT
									UL=$COLOR_PRIMARY
								else
									# active urgent desktop
									FG=$COLOR_DISABLED
									BG=$COLOR_ALERT
									UL=$COLOR_ALERT
								fi
								;;
						esac
						wm="${wm}%{F${FG}}%{B${BG}}%{U${UL}}%{+u}%{A:bspc desktop -f ${name}:} ${name} %{A}%{B-}%{F-}%{-u}"
						;;
					[LTG]*)
						# layout, state and flags
						wm="${wm}%{F$COLOR_SECONDARY} ${name} %{F-}"
						;;
				esac
				shift
			done
			;;
	esac
	echo "%{l}  ${pwr}  ${wm}%{c}${title}%{r}${volume}${sep}${backlight}${sep}${network}${sep}${battery}${sep}${clock}  "
done
